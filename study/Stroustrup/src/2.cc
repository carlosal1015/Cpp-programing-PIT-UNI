#include <iostream>

inline int mul(int a, int b) { return a * b; }

int main(int argc, char const *argv[])
{
  int a = 2, b = 3;

  std::cout << mul(2, 3) << std::endl;

  return 0;
}
