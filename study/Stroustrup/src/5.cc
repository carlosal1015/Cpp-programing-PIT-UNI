#include <iostream>

int printf(const char*, ...);
int fprintf(FILE*, const char* ...);
int sprintf(char*, const char* ...);

int main(int argc, char const *argv[])
{
  printf(stderr, "x = %d\n", x); // error: printf does not take a FILE*
  fprintf("x = %d\n", x); // error: fprintf needs a FILE*


  return 0;
}
