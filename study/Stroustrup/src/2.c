#include <stdio.h>

#define mul(a,b) a*b

int main(int argc, char const *argv[])
{

  int x = 1, y = 4;

  int z = mul(x*3+2,y/4);

  printf("El valor de z es %d", z);

  return 0;
}
