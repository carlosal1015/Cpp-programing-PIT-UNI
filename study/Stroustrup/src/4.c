#include <stdio.h>

void print(char *, char * = "-", char * = "-");

int main(int argc, char const *argv[])
{
  print("one", "two", "three");
  print("one", "two"); // that is, print("one", "two", "-");
  print("one"); // that is, print("one", "-", "-");
  return 0;
}
