#include <iostream>

int main(int argc, char const *argv[])
{

  class complex
  {
    double re, im public : complex(double r, double i)
    {
      re = r;
      im = i;
    }
    complex(double r)
    {
      re = r;
      im = 0;
    } // float-> complex conversion

    friend complex operator+(complex, complex);
    friend complex operator-(complex, complex); // binary minus
    friend complex operator-(complex);          // unary minus
    friend complex operator*(complex, complex);
    friend complex operator/(complex, complex);
    // ...
  };

  return 0;
}
