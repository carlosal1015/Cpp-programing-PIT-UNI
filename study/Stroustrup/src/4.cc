#include <iostream>

int main(int argc, char const *argv[])
{

  overload print;
  void print(int);
  void print(char*);

  print(1); // integer print function
  print("two"); // string print function


  return 0;
}
