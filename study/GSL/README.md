### First way

```console
$ gcc -Wall -I/usr/include -c 2.c
$ gcc -L/usr/local/lib 2.o -lgsl -lgslcblas -lm
$ ./a.out
```

### Second way

```console
$ gcc -Wall -I/usr/include -c 2.c
$ gcc 2.o -lgsl -lcblas -lm
$ ./a.out
```

### Third way

[](https://aur.archlinux.org/packages/atlas-lapack)

```console
$ gcc -Wall -I/usr/include -c 2.c
$ gcc 2.o -lgsl -lcblas -lm
$ ./a.out
```
