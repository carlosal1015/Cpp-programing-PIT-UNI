#include <tabulate/table.hpp>
using namespace tabulate;

using Row_t = std::vector<variant<std::string, const char *, Table>>;

int main(int argc, char const *argv[])
{

  // Instantiate table object
  Table table;

  std::cout << "Hello table" << table << std::endl;

  table.format().width(20);
  table.add_row(Row_t{"Hola"});

  std::cout << table << std::endl;

  return 0;
}
