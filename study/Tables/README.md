## Cmake docs

- [`cmake_minimum_required`](https://cmake.org/cmake/help/v3.19/command/cmake_minimum_required.html)
- [`project`](https://cmake.org/cmake/help/v3.19/command/project.html)
- [`option`](https://cmake.org/cmake/help/v3.19/command/option.html)
- [`set`](https://cmake.org/cmake/help/v3.19/command/set.html)
- [`file`](https://cmake.org/cmake/help/v3.19/command/file.html)
- [`foreach`](https://cmake.org/cmake/help/v3.19/command/foreach.html)
- [`if`](https://cmake.org/cmake/help/v3.19/command/if.html)
- [`message`](https://cmake.org/cmake/help/v3.19/command/message.html)
- [`string`](https://cmake.org/cmake/help/v3.19/command/string.html)
- [`include`](https://cmake.org/cmake/help/v3.19/command/include.html)
- [`add_library`](https://cmake.org/cmake/help/v3.19/command/add_library.html)
- [`target_compile_features`](https://cmake.org/cmake/help/v3.19/command/target_compile_features.html)
- [`target_include_directories`](https://cmake.org/cmake/help/v3.19/command/target_include_directories.html)
- [`add_subdirectory`](https://cmake.org/cmake/help/v3.19/command/add_subdirectory.html)
- [`configure_package_config_file`](https://cmake.org/cmake/help/v3.19/module/CMakePackageConfigHelpers.html#command:configure_package_config_file)
- [`write_basic_package_version_file`](https://cmake.org/cmake/help/v3.19/module/CMakePackageConfigHelpers.html#command:write_basic_package_version_file)
- [`install`](https://cmake.org/cmake/help/v3.19/command/install.html)

## Extra

- [What is the difference between `include_directories` and `target_include_directories` in CMake?](https://stackoverflow.com/a/31969632/9302545)


#### `cmake_minimum_required`

Para fijar la versión mínima de cmake, en este caso la que contamos es `3.19.6`.
También se puede especificar la versión mínima y máxima respectivamente.

#### `project`

Fija el nombre del proyecto y lo almacena en la variable `PROJECT_NAME`.

#### `option`

Podemos cambiar una variable a verdadero o falso, por defecto es falso.

#### `set`

Existen tres tipos de variables: `normal`, `cache` y `environment`.
