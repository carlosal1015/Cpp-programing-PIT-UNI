---
title: Segunda clase
date: 2021/3/12 08:00:00
updated: 2021/3/12 17:00:00
tags:
  - c
  - c++
categories:
  - inicio
top_image: https://www.tutorialandexample.com/wp-content/uploads/2018/07/c-formatted-input-output-function-min.jpg
---

## Hola

Hola

<!-- more -->

$\sum\limits_{i=0}^{n}i^2$

$$
x = {-b \pm \sqrt{b^2-4ac} \over 2a}
$$
