C++ es un lenguaje de propósito general diseñado para hacer la programación más agradable para el programador serio.

C++ es un superconjunto del lenguaje C.

C++ fue diseñado para ser mejor que C, soportar [abstracción de datos](https://de.wikipedia.org/wiki/Abstraktion_(Informatik)), ignorar los detalles de la implementación y derivar un esquema general para resolver el problema a partir de ella, y soporte la programación orientada a objetos.

C++ fue influenciado por C, Simula67 (un lenguaje orientado a objetos) y Algol68.

Es utilizado en la construcción de compiladores, administración de bases de datos, gráficos, procesamiento de imágenes, sintetizador de música, redes, software numérico, entornos de programación, robótica y simulación.

## Comprobación del tipo de argumento y coerción
