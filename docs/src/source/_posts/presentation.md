---
title: Presentación
date: 2021/3/10 08:00:00
updated: 2021/3/12 17:00:00
tags:
  - c
  - c++
categories:
  - inicio
top_image: https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&amp;cs=tinysrgb&amp;dpr=2&amp;h=750&amp;w=1260
---

¡Bienvenido al [blog](https://carlosal1015.gitlab.io/Cpp-programing-PIT-UNI)! Estas son las notas de clases del egresado de matemática de la Facultad de Ciencias, Abraham Zamudio Chauca, pero se ha enfocado en el desarrollo de software. ¡Muy bien! {% github_emoji sparkles %}

<!-- more -->

## ¿De qué trata el curso?

Es un curso introductorio y rápido, pero pensado para mostrar las características básicas del [lenguaje C++](https://es.wikipedia.org/wiki/C%2B%2B) y las diferencias en las versiones de la misma sintáxis del lenguaje. Entonces, a partir de estas ideas básicas se desea adquirir la habilidad de aprender por sí solos y comenzar a avanzar leyendo los libros de [Bjarne Stroustrup](https://es.wikipedia.org/wiki/Bjarne_Stroustrup). Se tiene la intención de llegar hasta los vectores de la [biblioteca estándar de C++](https://es.wikipedia.org/wiki/Biblioteca_est%C3%A1ndar_de_C%2B%2B).

## Un poco de historia

El [lenguaje C](https://es.wikipedia.org/wiki/C_(lenguaje_de_programaci%C3%B3n)) históricamente viene de [ALGOL](https://es.wikipedia.org/wiki/ALGOL) y a la vez, este fue influido por [Fortran](https://es.wikipedia.org/wiki/Fortran). En las computadoras antiguas se picaban tarjetas para mostrar los resultados.

![Fortran](http://www.herongyang.com/Computer-History/Fortran-Punch-Card.jpg)

C es un lenguaje antiguo, pero vamos a desarrollar la diferencia entre C y C++. Esto debe de quedar claro, porque cuando leamos un libro, artículo o tesis, por ejemplo, [DUNE — The Distributed and Unified Numerics Environment, Oliver Sander](https://www.springer.com/gp/book/9783030597016) donde haya código, se pueda interpretar muy bien esa naturaleza.

![](https://dune-project.org/img/dune-book-cover.png)

En los 80s, Fortran quedó relegado por C en el trabajo de vectores y matrices. ¿Por qué? Respuesta: Todo lo que hace la computadora tiene que tener una formulación matricial, por ejemplo, [bases de datos](https://es.wikipedia.org/wiki/Base_de_datos), [interfaz gráfica de usuario](https://es.wikipedia.org/wiki/Interfaz_gr%C3%A1fica_de_usuario), ejemplos de estos tenemos a la [biblioteca Qt](https://www.qt.io), [GTK](https://www.gtk.org/docs/getting-started/hello-world), entre otros; .
![Qt y GTK](https://i.ytimg.com/vi/y5q2Ucv0ktc/maxresdefault.jpg)

(vea la siguiente sección).

## [ANSI C](https://es.wikipedia.org/wiki/ANSI_C)

Esta organización

[Cloud computing](https://es.wikipedia.org/wiki/Computaci%C3%B3n_en_la_nube)

{% codeblock hello-world-gtk.c lang:c %}
#include <gtk/gtk.h>

static void
print_hello (GtkWidget *widget,
             gpointer   data)
{
  g_print ("Hello World\n");
}

static void
activate (GtkApplication *app,
          gpointer        user_data)
{
  GtkWidget *window;
  GtkWidget *button;

  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), "Window");
  gtk_window_set_default_size (GTK_WINDOW (window), 200, 200);

  button = gtk_button_new_with_label ("Hello World");
  g_signal_connect (button, "clicked", G_CALLBACK (print_hello), NULL);
  gtk_window_set_child (GTK_WINDOW (window), button);

  gtk_window_present (GTK_WINDOW (window));
}

int
main (int    argc,
      char **argv)
{
  GtkApplication *app;
  int status;

  app = gtk_application_new ("org.gtk.example", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (activate), NULL);
  status = g_application_run (G_APPLICATION (app), argc, argv);
  g_object_unref (app);

  return status;
}
{% endcodeblock %}

{% codeblock main.cc lang:c++ %}
#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
{% endcodeblock %}

## Lecturas adicionales

- [C++ 20 spec finalized, C++ 23 spec begins](https://www.infoworld.com/article/3528882/c-plus-plus-20-spec-finalized-c-plus-plus-23-spec-begins.html)
- []()
- []()
