---
title: Tabulate
date: 2021-03-15 20:18:14
top_image: https://raw.githubusercontent.com/p-ranav/tabulate/master/img/universal_constants.png
---

## Tabulate en C++

En este tutorial aprenderemos a imprimir tablas en la consola, usando [archivos de cabecera](https://es.wikipedia.org/wiki/Archivo_de_cabecera) `tabulate`. Explora su código fuente en [GitHub](https://github.com/p-ranav/tabulate). Por lo que usaremos `cmake` y `make` para poder compilar de manera sencilla.
Aquí debajo podemos ver un ejemplo minimal que no imprime una tabla, pero si logras compilarlo de manera correcta podrás seguir avanzando.

{% codeblock hello_table.cc lang:c++ %}
#include <tabulate/table.hpp>
using namespace tabulate;

using Row_t = std::vector<variant<std::string, const char *, Table>>;

int main(int argc, char const *argv[])
{

  return 0;
}
{% endcodeblock %}
Este es un archivo `CMakeLists.txt` que funciona para este ejemplo.
{% codeblock CMakeLists.txt mark:8 %}
cmake_minimum_required(VERSION 3.19.6)
project(tables_in_c++)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR x86_64)
set(CMAKE_C_COMPILER clang)
set(GCC_COVERAGE_COMPILE_FLAGS "-Wall")
include_directories(../tabulate/include)

add_executable(hello_table hello_table.cc)
{% endcodeblock %}
Debes asegurarte en la línea resultada que esté correctamente la ruta relativa (o absoluta) a la carpeta `include` que clonaste de GitHub.
Ahora, si dentro de tu carpeta `src` está el `CMakeLists.txt` y `hello_table.cc`, puedes ver un ejemplo [aquí](https://gitlab.com/carlosal1015/Cpp-programing-PIT-UNI/-/tree/main/study/Tables). Entonces, creas una carpeta vacía `build` y si ejecutas `cmake .. && make` encontrás un archivo llamado `hello_table`.

## Veamos la composición del programa

{% codeblock table.cc lang:c++ %}
class Table {
public:
  Table() : table_(TableInternal::create()) {}

  Table &add_row(const std::vector<variant<std::string, const char *, Table>> &cells) {

    if (rows_ == 0) {
      // This is the first row added
      // cells.size() is the number of columns
      cols_ = cells.size();
    }

    std::vector<std::string> cell_strings;
    if (cells.size() < cols_) {
      cell_strings.resize(cols_);
      std::fill(cell_strings.begin(), cell_strings.end(), "");
    } else {
      cell_strings.resize(cells.size());
      std::fill(cell_strings.begin(), cell_strings.end(), "");
    }

    for (size_t i = 0; i < cells.size(); ++i) {
      auto cell = cells[i];
      if (holds_alternative<std::string>(cell)) {
        cell_strings[i] = *get_if<std::string>(&cell);
      } else if (holds_alternative<const char *>(cell)) {
        cell_strings[i] = *get_if<const char *>(&cell);
      } else {
        auto table = *get_if<Table>(&cell);
        std::stringstream stream;
        table.print(stream);
        cell_strings[i] = stream.str();
      }
    }

    table_->add_row(cell_strings);
    rows_ += 1;
    return *this;
  }

  Row &operator[](size_t index) { return row(index); }

  Row &row(size_t index) { return (*table_)[index]; }

  Column column(size_t index) { return table_->column(index); }

  Format &format() { return table_->format(); }

  void print(std::ostream &stream) { table_->print(stream); }

  std::string str() {
    std::stringstream stream;
    print(stream);
    return stream.str();
  }

  std::pair<size_t, size_t> shape() { return table_->shape(); }

  class RowIterator {
  public:
    explicit RowIterator(std::vector<std::shared_ptr<Row>>::iterator ptr) : ptr(ptr) {}

    RowIterator operator++() {
      ++ptr;
      return *this;
    }
    bool operator!=(const RowIterator &other) const { return ptr != other.ptr; }
    Row &operator*() { return **ptr; }

  private:
    std::vector<std::shared_ptr<Row>>::iterator ptr;
  };

  auto begin() -> RowIterator { return RowIterator(table_->rows_.begin()); }
  auto end() -> RowIterator { return RowIterator(table_->rows_.end()); }

private:
  friend class MarkdownExporter;
  friend class LatexExporter;
  friend class AsciiDocExporter;

  friend std::ostream &operator<<(std::ostream &stream, const Table &table);
  size_t rows_{0};
  size_t cols_{0};
  std::shared_ptr<TableInternal> table_;
};
{% endcodeblock %}
