#include <iostream>
#include <math.h>

using namespace std;

// Funcion que calcula el epsilon de maquina
float epsilon()
{
  float eps1 = 1, prevEps1;
  // eps + a = a, eps != 0
  while ((eps1 + 12) != 12)
  {
    prevEps1 = eps1;
    // cout << "Convergencia : " << prevEps << endl;
    eps1 = eps1 / 2;
  }
  return eps1;
}

// Regla de correspondencia de la funcion
double func(double x)
{
  return pow(x, 3) - pow(x, 2) + 2;
}

// Regla de correspondencia de la derivada de la funcion
double derivFunc(double x)
{
  return 3 * pow(x, 2) - 2 * x;
}

/*
Implementacion del algoritmode Newton-Raphson
*/
void newtonRaphson(double x)
{
  /*
La formula deducida (LO IMPORTANTE ES LA DEDUCCION !!!) es :
x[i+1] = x[i] - (f(x[i]))/(f'(x[i]))
*/

  double h = func(x) / derivFunc(x);
  double eps = epsilon();
  while (abs(h) >= eps)
  {
    h = func(x) / derivFunc(x);
    x = x - h;
  }

  cout << "El valor aproximado de la raiz es " << x << endl;
  // la funcion devuelve nada (void)
  // notar que en este escenario ya no se utiliza return
}

int main()
{
  double x1;

  cout << "Ingresa el primer punto de la iteracion" << endl;
  cin >> x1;

  // llamada ala fucion newtonRaphson
  newtonRaphson(x1);

  return 666;
}
