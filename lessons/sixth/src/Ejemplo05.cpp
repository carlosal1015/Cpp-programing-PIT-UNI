
#include <iostream>
#include <cstring>

using namespace std;

int main()
{
  /*
Contar la cantidad de letras en una palabra o conjunto de caracteres (alfabeto)

Ayuda :
    strlen : cuenta la cantidad de elementos de una variable char
    isalpha : dado un caracter (de argumento), esta funcion verifica que el
        caracter pertenece al alfabeto

*/

  char str[68];
  int num = 0;

  cout << "Ingresa una cadena de caracteres " << endl;
  cout << "(Sin espacio en blanco)" << endl;
  cin >> str;

  // utilizamos una estructura repetitiva for para barrer todos
  // los elementos de la variable str

  for (int i = 0; i <= strlen(str); i++)
  {
    // con este for,se crea una secuencia de numeros enteros
    // quienes serviran como conjunto de indices de la estructura vectorial (char)
    // str
    if (isalpha(str[i]))
    { // verificamos que str[i] sea un elemento del alfabeto
      num = num + 1;
    }
  }

  cout << "Cadena ingresada " << str << endl;
  cout << "Numero de elementos del alfabeto " << num << endl;

  return 71;
}
