#include <iostream>
#include <math.h>

using namespace std;

int main()
{
  // Crear una variable de tipo char
  char cadena1[666];

  cout << "Ingresa una cadena de caracteres " << endl;
  cin >> cadena1;

  cout << "La variable ingresada es :  " << cadena1 << endl;

  // Obs: de esta manera solo almacenamos lo ingresado por el teclado
  // hasta encontrar un espacio en blanco

  return 68;
}
