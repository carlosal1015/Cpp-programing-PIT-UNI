#include <iostream>
#include <math.h>

using namespace std;

// Funcion que calcula el epsilon de maquina
float epsilon()
{
  float eps1 = 1, prevEps1;
  // eps + a = a, eps != 0
  while ((eps1 + 12) != 12)
  {
    prevEps1 = eps1;
    // cout << "Convergencia : " << prevEps << endl;
    eps1 = eps1 / 2;
  }
  return prevEps1;
}

// Definimos una funcion (continua) que calcula la regla de correspondencia
double func(double x)
{
  return pow(x, 3) - pow(x, 2) + 2;
}

//Definicion de una funcion "bisection" que muestra en pantalla
// una aproximacion a una raiz de una funcion continua
void bisection(double a, double b)
{
  if (func(a) * func(b) >= 0)
  {
    cout << "Los valores de a = " << a << " y b = " << b << " No son adecuados para nuestro algoritmo " << endl;
    return;
  }

  double c; // punto medio en cada iteracion del algoritmos
  double eps;
  eps = epsilon();

  // parte repetitiva : iniciamos verificando la longitud del intervalo de busqueda
  while ((b - a) >= eps)
  {
    c = (a + b) / 2; // punto medio de cada subintervalos

    // verifico que el punto medio es una raiz
    if (func(c) == 0)
    {
      break; // sale del while
    }
    // decido con cual subintervalo quedarme
    else if (func(c) * func(a) < 0)
    {
      b = c;
    }
    else
    {
      a = c;
    }
  } // finaliza el while

  cout << "El valor aproximado de la raiz es : " << c << endl;
  // NO utilizo return, pues la funcion devuelve algo de tipo void
}

int main()
{

  double var1 = -2;
  double var2 = 0;

  bisection(var1, var2);

  return 667;
}
