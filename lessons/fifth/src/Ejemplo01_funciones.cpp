
/*
Verifiquemos que un numero sea primo o no

*/

#include <iostream>
using namespace std;

bool EsPrimo(int n)
{ // inicio de la funcion
  bool primo = true;

  // Estructura IF para verificar casos en que n = 1 o n= 0
  if (n == 0 || n == 1)
  {
    primo = false;
  }
  else
  {
    // estructura repetitiva para dividir n por todos los numeros enteros
    // desde 2 hasta n/2
    for (int i = 2; i <= n / 2; ++i)
    {
      if (n % i == 0)
      {
        primo = false;
        break;
      } // fin del if
    }   // fin del for
  }     // fin del else

  return primo;
} // fin de la funcion EsPrimo

int main()
{
  int n;
  bool outputEsPrimo;

  // Ingresar el dato de entrada (input)
  cout << "Ingresa un numero entero [0, .... ]" << endl;
  cin >> n;

  // asigno la salida de mi funcion EsPrimo a una variable :
  // hay quetener una paridad entre el tipo de variable asignada y el tipo de variable que retorna la funcion
  outputEsPrimo = EsPrimo(n);

  if (outputEsPrimo)
  {
    cout << n << " es primo " << endl;
  }
  else
  {
    cout << n << " NO es primo " << endl;
  }
  return 11;
}
