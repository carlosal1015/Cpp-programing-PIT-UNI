#include <iostream>
using namespace std;

float epsilon()
{
  float eps1 = 1, prevEps1;

  // eps + a = a, eps != 0
  while ((eps1 + 12) != 12)
  {
    prevEps1 = eps1;
    // cout << "Convergencia : " << prevEps << endl;
    eps1 = eps1 / 2;
  }
  return prevEps1;
}

int main()
{
  float eps = 1, prevEps;
  float outputEpsilon;
  outputEpsilon = epsilon();

  // eps + a = a, eps != 0
  while ((eps + 12) != 12)
  {
    prevEps = eps;
    cout << "Convergencia : " << prevEps << endl;
    eps = eps / 2;
  }

  cout << "Epsilon de maquina (main): " << prevEps << endl;

  cout << "Epsilon de maquina (Funcion epsilon()): " << outputEpsilon << endl;

  return 666;
}
