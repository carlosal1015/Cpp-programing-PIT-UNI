/*
Verifiquemos que un numero sea primo o no

*/

#include <iostream>
using namespace std;

int main()
{
  int n, i;
  bool primo = true;

  // Ingresar el dato de entrada (input)
  cout << "Ingresa un numero entero [0, .... ]" << endl;
  cin >> n;

  // Estructura IF para verificar casos en que n = 1 o n= 0
  if (n == 0 || n == 1)
  {
    primo = false;
  }
  else
  {
    // estructura repetitiva para dividir n por todos los numeros enteros
    // desde 2 hasta n/2
    for (i = 2; i <= n / 2; ++i)
    {
      if (n % i == 0)
      {
        primo = false;
        break;
      } // fin del if
    }   // fin del for
  }     // fin del else

  if (primo)
  {
    cout << n << " es primo " << endl;
  }
  else
  {
    cout << n << " NO es primo " << endl;
  }

  return 11;
}
