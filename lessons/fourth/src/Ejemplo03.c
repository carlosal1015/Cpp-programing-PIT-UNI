#include <stdio.h>

/*
Vectores (arrays) son un conjunto finito e indexado de datos homogeneos
(del mismo tipo) con un nombre comun para todos ellos (nombre dela variable)
*/

int main()
{

  // definimos un vector de tipo float inicializando sus valores
  float precios[6] = {1.5, 3.5, 8.50, 0}; // llaves {}
  int i;                                  // variable entera para representar los indices de la variable vectorial

  printf("Visualizacion de los dos primeros elementos \nalmacenados en RAM \n");
  printf("La 1era comp. del vector precios es : %.2f \n", precios[0]);
  printf("La 2da comp. del vector precios es : %.2f \n", precios[1]);

  printf("La 6ta comp. del vector precios es : %.2f \n", precios[5]);

  printf("La 7ma comp. del vector precios es : %.2f \n", precios[6]);

  return 666;
}
