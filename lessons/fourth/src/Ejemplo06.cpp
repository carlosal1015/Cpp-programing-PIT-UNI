#include <iostream>
#include <math.h>
using namespace std;

/*
Producto interno de dos vectores
*/
int main()
{
  int i; // variable contadora
  int n; // cantidad de elementos del primer vector
  int m; // cantidad de elementos del segundo vector

  // Cantidad de elementos del 1er vector
  cout << "Ingrese un numero entero " << endl;
  cin >> n;

  // Cantidad de elementos del 2do vector
  cout << "Ingrese un numero entero " << endl;
  cin >> m;

  // declaramos los vectores    :
  float a[n];
  float b[m];

  if (n != m)
  {
    cout << "El producto interno es inviable " << endl;
  }
  else
  { // caso en que m=n

    // primer vector : a
    for (i = 0; i < n; i++)
    {
      cout << "a [ " << i << " ]" << endl;
      cin >> a[i];
    }

    // segundo vector : b
    for (i = 0; i < n; i++)
    {
      cout << "b [ " << i << " ]" << endl;
      cin >> b[i];
    }

    // declaramos prodInt una variable donde almacenaremos el producto interno de a yy b
    float prodInt = 0.0; // variable acumuladora
    for (i = 0; i < n; i++)
    {
      prodInt = prodInt + a[i] * b[i];
    }
    cout << "El producto interno es : " << prodInt << endl;
  }

  return 666;
}
