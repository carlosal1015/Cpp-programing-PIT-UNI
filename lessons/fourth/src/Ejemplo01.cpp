/*
Estructura IF-ELSE
*/

#include <iostream>
#include <math.h>

using namespace std;

int main()
{
  /*
Muestra un mensajes : "Numero mayor o igual que cero" cuando  el numero es mayor o igual que cero
y el mansaje "Numero negativo" cuando el numero es negativo
*/

  int numero;

  cout << "Ingrese un numero " << endl;
  cin >> numero;

  if (numero >= 0)
  {
    cout << "Numero mayor o igual que cero" << endl;
  }
  else
  { // esto se ejecuta cuando (numero >= 0) tiene valor booleano FALSE
    cout << "Numero negativo" << endl;
  }

  cout << "El programa Termino" << endl;

  return 666;
}
