
/*
Estructura IF- ELSE IF - ELSE
*/

#include <iostream>
#include <math.h>

using namespace std;

int main()
{
  int numero;

  cout << "Ingrese un numero " << endl;
  cin >> numero;

  if (numero > 0)
  {
    cout << "El numero ingresado es mayor a cero " << endl;
  }
  else if (numero < 0)
  {
    cout << "El numero ingresado es menor a cero " << endl;
  }
  else
  {
    cout << "El numero ingresado es igual a cero " << endl;
  }
}
