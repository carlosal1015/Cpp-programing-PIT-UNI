#include <iostream>
#include <math.h>
using namespace std;

/*
Dado un numero entero n ingresado por el usuario, ingresar un vector de n componentes.
Calcular la media aritmetica, geometrica y armonica de este vector
*/

int main()
{
  int i, n;

  // Cantidad de elementos del vector
  cout << "Ingrese un numero entero " << endl;
  cin >> n;

  float data[n];
  /*
Observacion : La declaracion de la variable vectorial de n componentes es luego de
ingresar la variable n
*/
  // Este for es utilizado para el ingreso de datos (elementos del vector)
  for (i = 0; i < n; i++)
  {
    cout << "data [ " << i << " ]" << endl;
    cin >> data[i];
  }

  // VERIFICACION : imprimamos los valores ingresados
  for (i = 0; i < n; i++)
  {
    cout << "data [ " << i << " ]= " << data[i] << endl;
  }

  // calculo del promedio aritmetico
  float suma = 0.0;
  for (i = 0; i < n; i++)
  {
    suma = suma + data[i];
    // el nuevo valor de suma, es el valor anterior de suma mas la i-esima componente de data (vector)
  }
  cout << "La media aritmetica del vector de datos es : " << suma / n << endl;

  //calculo del promedio geometrico
  float prod = 1.0;
  for (i = 0; i < n; i++)
  {
    prod = prod * data[i];
    // el nuevo valor de prod es igual al valor anterior de prod multiplicado con la i-esima
    // componente de data (vector)
  }
  cout << "La media geometrica del vector de datos es " << pow(prod, 1.0 / n) << endl;

  // calculo de la media armonica
  float sumai = 0.0;
  for (i = 0; i < n; i++)
  {
    sumai = sumai + pow(data[i], -1);
  }
  cout << "La media armonica del vector de datos es " << n / sumai << endl;

  return 17;
}
