#include <iostream>
#include <math.h>
using namespace std;
/*
Definicion de funciones  :  las funciones devuelven un tipo de dato .
La funcion main es un claro ejemplo

Crear una funcion que calcule la imagen de un punto del dominio usando
la sgte regla de correspondencia :
f(x) = x^3 + x^2 + 2
*/

double func1(double x)
{
  return pow(x, 3) + x * x + 2;
}

int main()
{
  double var1 = 12.5;
  int i, j;
  cout << "La imagen de x = " << var1 << " mediante f(x)= x^3+x^2+2 es : " << func1(var1) << endl;

  // construir una tabla con las imagenes de 2^i (i = 0 ..100) evaluadas en func1
  for (i = 0; i <= 100; i++)
  {
    j = pow(2, i);
    cout << "f( " << j << " )= " << func1(j) << endl;
  }

  return 666;
}
