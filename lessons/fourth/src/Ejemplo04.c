
#include <stdio.h>

/*
Vectores (arrays) son un conjunto finito e indexado de datos homogeneos
(del mismo tipo) con un nombre comun para todos ellos (nombre dela variable)
*/

int main()
{

  // definimos un vector de tipo float inicializando sus valores
  float precios[6] = {1.5, 3.5, 8.50, 0};
  int i; // variable entera para representar los indices de la variable vectorial

  // VIsualizacion de las componentes del vector usando una estructura for
  for (i = 0; i < 6; i++)
  {
    // imprimimos cada una de las i-esimas componentes
    printf("Precios [%i] = %.2f \n", i, precios[i]);
  }

  return 666;
}
