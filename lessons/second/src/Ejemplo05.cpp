#include <iostream>

using namespace std;

int main()
{
  float x = 3.14159;
  double y = x / 2;
  int z = 317;
  cout << "El valor almacenado en x es " << x << "\n";
  cout << "El cuadrado de x es " << x * x << "\n"
       << "El cubo de x es " << x * x * x << endl;

  return 70;
}
