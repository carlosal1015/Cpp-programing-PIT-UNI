#include <iostream> // iostream : rutinas/funciones de input/output

/*
stdio : input/output (en C)
iostream : input/output (en C+) stream (flujo)
*/

using namespace std;

int main()
{

  // mo olvido printf : es para mostrar mensajes en pantalla
  // cout : c-out
  cout << "Segunda Clase de C++ : PIT2021 \n \n \n " << endl;

  return 666;
}
