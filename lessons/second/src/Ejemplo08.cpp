/*
Aproximacion del seno de x usando dos terminos  del polinomio de taylor
*/

#include <iostream>
#include <math.h>

using namespace std;

int main()
{
  float x;
  float Aprox_Sin;
  float error;
  cout << "Ingresa el valor de x " << endl;
  cin >> x;

  Aprox_Sin = x / 1 + pow(x, 3) / 6;
  error = abs(sin(x) - Aprox_Sin);

  cout.precision(3);
  cout << "El seno de x = " << x << " es : " << sin(x) << endl;
  cout << "La aproximacion usando taylos es " << Aprox_Sin << endl;
  cout << "El error cometido es : " << error << endl;

  return 10;
}
