#include <iostream>
using namespace std;

/*
pedir un numero entero al usuario de manera repetitiva , hasta que el numero
ingresado cumpla una condicion.
La condicion de parada es que el numero ingresado sea menor o igual que una constante
*/

#define BANDERA 100
int main()
{

  // declarar variables
  double numero;
  double copyNum;

  /*
me interesa contar (y sumar) los numeros que cumplen la condicion
 (sean mayores o iguales a BANDERA)
*/
  int NumVeces = 0;
  double suma = 0;

  // proceso de lectura de dato
  cout << "INgrese un numero " << endl;
  cin >> numero;

  while (numero >= BANDERA)
  {
    NumVeces = NumVeces + 1;
    suma = suma + numero;
    copyNum = numero;
    cout << "INgrese un numero " << endl;
    cin >> numero;
  }

  cout << "El ultimo numero ingresado es " << copyNum << endl;
  cout << "El numero de datos ingresados es " << NumVeces << endl;
  cout << "La suma de los datos ingresados es " << suma << endl;

  return 666;
}
