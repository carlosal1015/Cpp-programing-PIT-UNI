/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 *
 * This file is part of Cpp-programing-PIT-UNI.
 *
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

int main(int argc, char const *argv[])
{
    char ntrab[45];
    float sueldo, bonif, dscto, neto;

    std::cout << "Ingrese el nombre del trabajador: ";
    std::cin >> ntrab;

    std::cout << "Ingrese el sueldo básico: ";
    std::cin >> sueldo;

    bonif = 0.05 * sueldo;
    dscto = 0.02 * sueldo;
    neto = sueldo + bonif - dscto;

    std::cout << "Trabajador: " << ntrab;
    std::cout << "Sueldo: " << sueldo;
    std::cout << "Bonificación: " << bonif;
    std::cout << "Descuento: " << dscto;
    std::cout << "Sueldo neto: " << neto;

    return 666;
}
