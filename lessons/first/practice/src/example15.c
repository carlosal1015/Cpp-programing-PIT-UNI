/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 * 
 * This file is part of Cpp-programing-PIT-UNI.
 * 
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

int main(int argc, char const *argv[])
{
  float tc, d, tp;
  printf("\nIngrese la compra: ");
  scanf("%f", &tc);
  d = tc * .15;
  tp = tc - d;
  printf("\nEl total de la compra es: %f", tp);

  return 0;
}
