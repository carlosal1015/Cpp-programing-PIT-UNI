/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 *
 * This file is part of Cpp-programing-PIT-UNI.
 *
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

int i = 1, j = 2, k = 3; /* Globales */

int main(int argc, char const *argv[])
{
  int i = 4,
      j = 5,
      k = 6;
  { /* Bloque */
    int i = 7, j = 8, k = 9;
    printf("Locales al bloque: %d %d %d\n", i, k, k);
  }
  printf("Locales a main ( ): %d %d %d\n", i, j, k);
  return 0;
}
