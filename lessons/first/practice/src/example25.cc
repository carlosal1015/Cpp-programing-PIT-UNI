/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 *
 * This file is part of Cpp-programing-PIT-UNI.
 *
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

int main(int argc, char const *argv[])
{
    int x = 317;
    std::cout << "El número 'x' es " << x << "." << std::endl;
    std::cout << "Su cuadrado y su cubo son" << x * x << "y"
              << x * x * x << " respectivamente." << std::endl;
    std::cout << "El polinomio 25x^3 + 12x^2 - 8x + 2 elevado a 'x' da"
              << 25 * x * x * x + 12 * x * x - 8 * x + 2 << std::endl;

    return 0;
}
