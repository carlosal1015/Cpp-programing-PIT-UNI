/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 *
 * This file is part of Cpp-programing-PIT-UNI.
 *
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Calcula el área de un triángulo conociendo sus tres lados */

#include <iostream>
#include <cmath> // Reconoce la raíz "sqrt"

int main(int argc, char const *argv[])
{
    float a, b, c; /* Se define el tipo de dato_entrada */
    float p, at;   /* Se define el tipo de dato_entrada */

    std::cout << "Ingrese el valor del lado a: ";
    std::cin >> a;
    std::cout << "Ingrese el valor del lado b: ";
    std::cin >> b;
    std::cout << "Ingrese el valor del lado c: ";
    std::cin >> c;

    p = (a + b + c) / 2.0;
    at = sqrt(p * (p - a) * (p - b) * (p - c));
    std::cout << at;

    return 0;
}
