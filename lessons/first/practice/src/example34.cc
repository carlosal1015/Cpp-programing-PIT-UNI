/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 *
 * This file is part of Cpp-programing-PIT-UNI.
 *
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cmath>         /* Reconoce el pow(base, exponente) */
#define A (M_PI) / 180.0 /*Se define a "A" como el valor de conversión */

int main(int argc, char const *argv[])
{
    float f1, f2, anggrados;
    float R, anggradian;

    std::cout << "Ingrese el valor de f1: ";
    std::cin >> f1;

    std::cout << "Ingrese el valor de f2: ";
    std::cin >> f2;

    std::cout << "Ingrese el valor del ángulo entre f1 y f2";
    std::cin >> anggrados;

    R = sqrt(pow(f1, 2) + pow(f2, 2) + 2 * f1 * f2 * cos(anggradian));
    std::cout << R;

    return 0;
}
