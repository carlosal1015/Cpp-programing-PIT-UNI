/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 *
 * This file is part of Cpp-programing-PIT-UNI.
 *
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

int main(int argc, char const *argv[])
{
  long int s, seg, min, hor, dia, sem;
  scanf("%ld", &s);
  seg = s % 60;
  s = s / 60;
  min = s % 60;
  s = s / 60;
  hor = s % 24;
  s = s / 60;
  dia = s % 7;
  s = s / 7;
  sem = s;
  printf("\n%ld semanas: %ld días: %ld horas: %ld minutos: %ld segundos", sem, dia, hor, min, seg);

  return 0;
}
