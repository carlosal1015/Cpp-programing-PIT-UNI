/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 * 
 * This file is part of Cpp-programing-PIT-UNI.
 * 
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#define CUATRO 4.0
float radio;

int main(int argc, char const *argv[])
{
  const float PI = 3.14159;
  float area, volumen;
  printf("Dé el radio de la esfera: ");
  scanf("%f", &radio);
  area = CUATRO * PI * radio * radio;
  printf("Área = %f\n", area);
  volumen = area * (radio / 3);
  printf("Volumen = %f\n", volumen);

  return 0;
}
