/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 *
 * This file is part of Cpp-programing-PIT-UNI.
 *
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

int main(int argc, char const *argv[])
{
    int sueldo, d100 = 0, d50 = 0, d20 = 0, d10 = 0, d5 = 0, d2 = 0, d1 = 0;

    std::cout << "Ingrese el sueldo: ";
    std::cin >> sueldo;

    // ¿Cuántos billetes de 100 recibirá?
    d100 = int(sueldo / 100);
    sueldo = sueldo - 100 * d100;
    // ¿Cuántos billetes de 50 recibirá?
    d50 = int(sueldo / 50);
    sueldo = sueldo - 50 * d50;
    // ¿Cuántos billetes de 20 recibirá?
    d20 = int(sueldo / 20);
    sueldo = sueldo - 20 * d20;
    // ¿Cuántos billetes de 10 recibirá?
    d10 = int(sueldo / 10);
    sueldo = sueldo - 10 * d10;
    // ¿Cuántas monedas de 5 recibirá?
    d5 = int(sueldo / 5);
    sueldo = sueldo - 5 * d5;
    // ¿Cuántas monedas de 2 recibirá?
    d2 = int(sueldo / 2);
    sueldo = sueldo - 2 * d2;

    d1 = sueldo;

    std::cout << "Billetes de 100: " << d100 << std::endl;
    std::cout << "Billetes de 50: " << d50 << std::endl;
    std::cout << "Billetes de 20: " << d20 << std::endl;
    std::cout << "Billetes de 10: " << d10 << std::endl;
    std::cout << "Monedas de 5: " << d5 << std::endl;
    std::cout << "Monedas de 2: " << d2 << std::endl;
    std::cout << "Monedas de 1: " << d1 << std::endl;

    return 666;
}
