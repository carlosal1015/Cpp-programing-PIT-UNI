/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 *
 * This file is part of Cpp-programing-PIT-UNI.
 *
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <cmath> /* Reconoce exp(X), se refiere al "e a la X" */

int main(int argc, char const *argv[])
{
    float t;
    double x, y, z;

    std::cout << "Ingrese el valor de t: ";
    std::cin >> t;

    x = pow(t, 3) - 8 * t + 4;
    y = sin(t) + cos(2 * t);
    z = exp(3 * t + 7);

    std::cout << "El valor de x es: " << x;
    std::cout << "\nEl valor de y es: " << y; /* "\n" se utiliza para bajar de línea */
    std::cout << "\nEl valor de z es: " << z;

    return 0;
}
