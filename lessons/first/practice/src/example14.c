/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 * 
 * This file is part of Cpp-programing-PIT-UNI.
 * 
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

int main(int argc, char const *argv[])
{
  float sb, v1, v2, v3, com, tot_ven, tot_pag;
  sb = 5000;
  printf("\nIngrese la venta 1: ");
  scanf("%f", &v1);
  printf("\nIngrese la venta 2: ");
  scanf("%f", &v2);
  printf("\nIngrese la venta 3: ");
  scanf("%f", &v3);
  tot_ven = v1 + v2 + v3;
  com = tot_ven * .10;
  tot_pag = sb + com;
  printf("\nTotal de pago: %f", tot_pag);
  printf("\nComisión: %f", com);

  return 0;
}
