/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 *
 * This file is part of Cpp-programing-PIT-UNI.
 *
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

const float precio = 0.45;

int main(int argc, char const *argv[])
{
    char contrato[15];
    float mc, importe, relleno, mantgral, igv, ptotal;

    std::cout << "Ingrese el contrato: " << std::endl;
    std::cin >> contrato;

    std::cout << "Ingrese el consumo de agua: " << std::endl;
    std::cin >> mc;

    importe = precio * mc;
    mantgral = importe * 0.028;
    relleno = importe * 0.014;
    igv = 0.19 * (importe + mantgral + relleno);
    ptotal = importe + mantgral + relleno + igv;

    std::cout << "Contrato: " << contrato << std::endl;
    std::cout << "Importe: " << importe << std::endl;
    std::cout << "Mantenimiento: " << mantgral << std::endl;
    std::cout << "Relleno: " << relleno << std::endl;
    std::cout << "Igv: " << igv << std::endl;
    std::cout << "Pago total: " << ptotal << std::endl;
    std::cout << " " << std::endl;

    return 0;
}
