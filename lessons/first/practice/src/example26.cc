/**
 * Copyleft (C) 2021 Oromion <caznaranl@uni.pe>
 *
 * This file is part of Cpp-programing-PIT-UNI.
 *
 * Cpp-programing-PIT-UNI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Cpp-programing-PIT-UNI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Cpp-programing-PIT-UNI.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>

int main(int argc, char const *argv[])
{
    std::cout << "Elige un número 'n'." << std::endl;
    int n = 74;
    std::cout << "[elijo el " << n << "]" << std::endl;
    n = 2 * n;
    std::cout << "[me da " << n << "]" << std::endl;
    std::cout << "Súmale 6." << std::endl;
    n = n + 6;
    std::cout << "[obtengo " << n << "]" << std::endl;
    n = n / 2 - 3;
    std::cout << "[sorpresa! obtengo el número inicial, " << n << "]" << std::endl;

    return 0;
}
