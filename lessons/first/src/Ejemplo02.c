// Agregamos comentarios de una linea
// Lo primero es agregar archivos cabecera (header)
#include <stdio.h> // stdio : standar input/output

// variables globales
int i;
int j;
int c, r;
int d = 12;
float R_global = 10.623; // numeros punto flotante precision simple
double K_Global = 3.159; // numeros punto flotante presicion doble

int main()
{
  // variables locales
  float k1, k2;
  int r1, r2 = 9;

  return R_global;
}
