// Agregamos comentarios de una linea
// Lo primero es agregar archivos cabecera (header)
#include <stdio.h> // stdio : standar input/output

/*
Nano Problema .
Dado el radio (r) leido por teclado
Calcular el area del a circunferencia y el volumen de la esfera de radio r
*/

#define CUATRO 4
float radio;

int main()
{
  const float PI = 3.14159;
  const int uno = 1;
  float area, volumen;

  // Proceso de lectura
  printf("Ingresa el radio : ");
  scanf("%f", &radio);

  // prueba
  printf("El valor ingresado en radio es %f \n\n", radio);

  // Calculos :
  area = CUATRO * PI * radio * radio;
  volumen = area * (radio / 3);

  printf("El area es %f \n", area);
  printf("El volumen es %f \n ", volumen);

  return 666;
}
