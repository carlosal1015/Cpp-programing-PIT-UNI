// Agregamos comentarios de una linea
// Lo primero es agregar archivos cabecera (header)
#include <stdio.h> // stdio : standar input/output

// definicion de constantes globales
#define LONGITUD 32

int main()
{
  int l1 = LONGITUD;
  int x = LONGITUD + 1;
  printf("El valor de x es %d ", x);

  return 0;
}
