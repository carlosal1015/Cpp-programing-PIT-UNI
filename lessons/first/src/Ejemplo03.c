// Agregamos comentarios de una linea
// Lo primero es agregar archivos cabecera (header)
#include <stdio.h> // stdio : standar input/output

int main()
{
    // variables locales
    int a, b, c;

    { // bloque de codigo
        int i = 4;
        int j = i + 2;
        float k = 1.0 * i / j;

        printf(" Locales al bloque de codigo : %d %d %f \n", i, j, k);
    }

    return 666;
}
