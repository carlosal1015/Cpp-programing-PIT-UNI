/*
file.c : codigo en lenguaje C en cuyo caso usamos la libreria stdio.h (standar input/output)
file.cpp : codigo en lenguaje C++ en cuyo caso usamos la  libreria iostream ( input/output stream)
*/

// archivos cabecera (header)
#include <iostream>
#include <math.h>
#include <string.h>

using namespace std;

/*
Las funciones en C o C++ siempre deben devolver un tipo de dato (int, float, double, char) escalar
Existe el caso en el cual la funcion devuelve nada (void)
* Este comportamiento depende de la version del compilador*
Por ejemplo :
void main(void){


}

 */
int main()
{ // llave inicia la funcion main

  /*
Dado un numero ingresado por teclado
imprimamos todas sus potencias desde 2 hasta 128
*/

  // definir nuestras variables
  int num; // ingresado por el usuario
  long int potencia;
  int i = 2; // inicializamos el valor de la variable i en 2

  // Lectura (ingreso) de los datos : variable num
  cout << "Ingresa la variable num (entero)" << endl;
  cin >> num;

  cout << "Inicia la estructura repetitiva while (mientras)" << endl;

  // calculos u operaciones
  while (i < 129)
  { // evaluamos una "condicion booleana" dentro del while
    potencia = pow(num, i);
    cout << i << " - " << potencia << endl;
    i = i + 1; // actualiza el valor de i ("condicion booleana")

    /*
    Para evitar bucles (loops) infinitos siempre se
    actualiza la variable de control (i)
    */
  }

  cout << "Termina la estructura repetitiva while (mientras)" << endl;

  return 666;
} // finaliza la funcion main
