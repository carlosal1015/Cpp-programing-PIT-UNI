#include <iostream>
#include <math.h>
#include <string.h>

using namespace std;

int main()
{
  int i; // contador dentro de  la estructura for

  // sintaxis
  for (i = 1; i <= 129; i = i + 2) // i++ = i = i+1
  {
    cout << "i = " << i << endl;
    cout << "i^2 = " << pow(i, 2) << endl;
    cout << "\n";
  }

  return 70;
}
