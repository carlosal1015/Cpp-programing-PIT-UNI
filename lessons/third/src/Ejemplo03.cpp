#include <iostream>
#include <math.h>
#include <string.h>

using namespace std;

int main()
{
  /*
ingresar una variable de tipo entero, en la cual almacenaremos la cantidad de datos a ingresar
Calcular el promedio de todos los datos ingresados
*/

  // declaramos variables
  int n;             // almacenaremos el numero de datos a ingresar por teclado
  int i;             // variable de iteracion (servir para las estructuras repetitivas)
  double suma = 0.0; // almacenaremos la suma de todos los datos ingresados
                     // IMPORTANTE : para variables donde se acumulen valores (resultados)
                     // es importante inicializarlas en cero (0 o 0.0)
  double dato;       // cada uno de los datos ingresados por el usuario (dentro de la estructura for)

  // Proceso de lectura (input) de los datos
  cout << "Ingrese un numero entero (cantidad de datos)" << endl;
  cin >> n;
  cout << "n = " << n << endl;

  /*
conforme voy leyendo los datos del teclado, voy acumulando estos en la variable suma
*/

  for (i = 1; i <= n; i++)
  {
    cout << "dato [" << i << "]" << endl;
    cin >> dato;

    //actualizo el valor de mi variable acumuladora : suma
    suma = suma + dato;
    /*
    El nuevo valor de suma , es el valor anterior de suma mas dato (recien ingresado por teclado)
    */
  }
  cout << "El promedio de los " << n << "datos ingresados es : " << suma / n << endl;

  return 113;
}
