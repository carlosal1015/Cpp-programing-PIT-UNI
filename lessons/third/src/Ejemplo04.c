// Estructura IF (seleccion)

#include <stdio.h>
#include <stdbool.h> // libreria para tener acceso al tipo de dato bool

int main()
{
  // declaracion de variables de tipo bool
  bool mensaje;
  bool caso1 = true;
  bool caso2 = false;
  int hora;

  /*
pedir al usuario que ingrese un numero entero que representa la hora del dia
y que el programa devuelva un mensaje
[0,12[ : "buenos dias"
[12,18[ : "buenas tardes"
[18,24[ : "buenas noches"
*/

  // proceso de lectura
  // 1ero vimos la necesidad de usar una estructura repetitiva while, pues no se conoce
  // apriori la cantidad de veces que el usuario pueda errar el ingreso de la variable

  printf("Ingrese la hora (numero entero) \n");
  scanf("%i", &hora);
  // %i, %d : especifican variables de tipo int

  while (hora < 0 || hora > 24)
  {
    printf("Ingrese la hora (numero entero) \n");
    scanf("%i", &hora);
  }

  if ((hora >= 0) && (hora < 12))
  {
    printf("BUenos dias \n");
  }

  if ((hora >= 12) && (hora < 18))
  {
    printf("BUenos tardes \n");
  }

  if ((hora >= 18) && (hora < 24))
  {
    printf("BUenos noches \n");
  }

  return 68;
}
