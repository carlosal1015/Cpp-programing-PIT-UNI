# Introducción a la programación en C++

<p align="center">
  <img src="./img/PIT_program_2021.jpg" width="750">
</p>

**Instructor**: [Abraham Gerardo Zamudio Chauca](http://directorio.concytec.gob.pe/appDirectorioCTI/VerDatosInvestigador.do;jsessionid=14ef7cf44595dc605c01234ab9de?id_investigador=94090)

**E-mail :email:**: [abraham.zamudio@uni.pe](mailto:abraham.zamudio@uni.pe)

**Aula**: Modadlidad virtual [Centro de Tecnologías de la Información y Comunicaciones](http://www.ctic.uni.edu.pe) de la [Universidad Nacional de Ingeniería](http://www.uni.edu.pe).

**Horario**: Lunes, miércoles y viernes desde las 8:00 hrs hasta las 10:00 hrs.

**Requisito mínimo para no ser penalizado**: _80% de asistencia_ de las clases totales.

## Cronograma de clases

| Sesión |   Fecha    |                                                                     Temas                                                                     |
| :----: | :--------: | :-------------------------------------------------------------------------------------------------------------------------------------------: |
|   1    | 10/03/2021 | [Un poco de historia de C, lenguajes interpretados y compilados, cómo crear un programa en C, características de C, compilación, elementos del lenguaje, variables y constantes, tipos de datos en C, operadores aritméticos, de relación, lógicos](gitlab.com:carlosal1015/Cpp-programing-PIT-UNI). |
|   2    | 12/03/2021 |                            [Data and C](gitlab.com:carlosal1015/Cpp-programing-PIT-UNI)                            |
|   3    | 15/03/2021 |                [Character strings and Formatted I/O](https://gitlab.com:carlosal1015/Cpp-programing-PIT-UNI)                |
